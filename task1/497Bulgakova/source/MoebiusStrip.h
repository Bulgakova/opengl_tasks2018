#include <iostream>
#include <algorithm>
#include <cmath>

#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>


class MoebiusStrip : public Application
{
public:
    MoebiusStrip() : delta_u(0.1), delta_v(0.1), aa(1.0) {}

    void makeScene() override;
    void update() override { Application::update(); }
    void draw() override;
    void handleKey(int key, int scancode, int action, int mods) override;

private:
	float delta_u;
    float delta_v;
	float aa;
    glm::vec3 countVertex(double u, double v);
    glm::vec3 countNormal(double u, double v);
    MeshPtr surface;
    ShaderProgramPtr shader;
};