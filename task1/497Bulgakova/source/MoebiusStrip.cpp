#include "MoebiusStrip.h"

void MoebiusStrip::makeScene()
{
    Application::makeScene();

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    for( double u = -0.4; u < 0.4; u += delta_u) {
        for( double v = 0; v < 2.0 * glm::pi<double>(); v += delta_v ) {
            double next_v_triangle = v + delta_v > 2.0 * glm::pi<double>() ? 2.0 * glm::pi<double>() : v + delta_v;
            double next_u_triangle = u + delta_u > 0.4 ? 0.4 : u + delta_u;

			//Первый треугольник, образующий квад
            vertices.push_back(countVertex(u, v));
            vertices.push_back(countVertex(u, next_v_triangle));
            vertices.push_back(countVertex(next_u_triangle, v));

            normals.push_back(countNormal(u, v));
            normals.push_back(countNormal(u, next_v_triangle));
            normals.push_back(countNormal(next_u_triangle, v));
			
			//Второй треугольник, образующий квад
			vertices.push_back(countVertex(u, next_v_triangle));
            vertices.push_back(countVertex(next_u_triangle, v));
            vertices.push_back(countVertex(next_u_triangle, next_v_triangle));
			
            normals.push_back(countNormal(u, next_v_triangle));
            normals.push_back(countNormal(next_u_triangle, v));
            normals.push_back(countNormal(next_u_triangle, next_v_triangle));
        }
    }

    DataBufferPtr bufVertices = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    bufVertices->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr bufNormals = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    bufNormals->setData(normals.size() * sizeof(float) * 3, normals.data());

    surface = std::make_shared<Mesh>();
    surface->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, bufVertices);
    surface->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, bufNormals);

    surface->setPrimitiveType(GL_TRIANGLES);
    surface->setVertexCount(vertices.size());

    std::cout << "Surface is created with " << vertices.size() << " vertices\n";

    shader = std::make_shared<ShaderProgram>();
    shader->createProgram("./497BulgakovaData/shaderNormal.vert", "./497BulgakovaData/shader.frag");
}

void MoebiusStrip::draw()
{
    Application::draw();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Устанавливаем шейдер
    shader->use();

    //Устанавливаем общие юниформ-переменные
    shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

    shader->setMat3Uniform("normalToCameraMatrix",
                           glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * surface->modelMatrix()))));

    //Рисуем меш
    shader->setMat4Uniform("modelMatrix", surface->modelMatrix());
    surface->draw();

}

void MoebiusStrip::handleKey(int key, int scancode, int action, int mods)
{
	Application::handleKey(key, scancode, action, mods);

	const double multiplier = 0.5;

	if (action == GLFW_PRESS) {
		if (key == GLFW_KEY_MINUS) {
			delta_u *= (1.0 / multiplier);
			delta_v *= (1.0 / multiplier);
			makeScene();
		}
		if (key == GLFW_KEY_EQUAL) {
			delta_u *= multiplier;
			delta_v *= multiplier;
			makeScene();
		}
	}
}

glm::vec3 MoebiusStrip::countVertex( double u, double v ) {
	double x = aa *  (cos(v) + u * cos(v / 2) * cos(v));
	double y = aa *  (sin(v) + u * cos(v / 2) * sin(v));
	double z = aa * u * sin(v / 2);
	
    return glm::vec3({x, y, z});
}

glm::vec3 MoebiusStrip::countNormal( double u, double v ) {
    double x_dev_u = aa * ( cos( v / 2 ) * cos(v) );
    double x_dev_v = aa * ( -0.5 * u * sin( v / 2 ) * cos(v) - u * sin(v) * cos( v / 2 ) - sin(v) );
    double y_dev_u = aa * ( sin(v) * cos( v / 2 ) );
    double y_dev_v = aa * ( 0.25 * u * cos( v / 2 ) + 0.75 * u * cos( 3 * v / 2 ) + cos(v) );
    double z_dev_u = aa * sin( v / 2 );
    double z_dev_v = aa * ( 0.5 * u * cos( v / 2 ) );

    glm::vec3 dev_u = glm::vec3( x_dev_u, y_dev_u, z_dev_u );
    glm::vec3 dev_v = glm::vec3( x_dev_v, y_dev_v, z_dev_v );
	
    return glm::normalize( glm::cross( dev_u, dev_v ) );
}