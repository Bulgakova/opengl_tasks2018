#version 330

uniform sampler2D diffuseTex;

in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

void main()
{
	vec3 color = texture(diffuseTex, texCoord).rgb;

	if (!gl_FrontFacing){
		fragColor = vec4(color, 1.0);
	} else {
		fragColor = vec4(1, 1, 0, 1.0);
	}
}