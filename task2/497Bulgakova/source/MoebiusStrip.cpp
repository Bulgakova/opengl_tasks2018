#include "MoebiusStrip.h"

const double MoebiusStrip::EPSILON = 0.000001;


std::vector<glm::vec4> points{ { 0, 0, 0, 1 },{ 0, 1, 0, 1 },{ 0, 0, 1, 1 },{ 0, 1, 1, 1 },{ 1, 1, 1, 1 } };

MeshPtr MoebiusStrip::makeMirror() 
{
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> texcoords;

	vertices.push_back(points[0]);
	vertices.push_back(points[1]);
	vertices.push_back(points[2]);

	vertices.push_back(points[2]);
	vertices.push_back(points[1]);
	vertices.push_back(points[3]);

	std::vector<glm::vec2> tc{ { 0,0 },{ 1,0 },{ 0, 1 },{ 1,1 } };

	texcoords.push_back(tc[0]);
	texcoords.push_back(tc[1]);
	texcoords.push_back(tc[2]);

	texcoords.push_back(tc[2]);
	texcoords.push_back(tc[1]);
	texcoords.push_back(tc[3]);

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

	MeshPtr mesh = std::make_shared<Mesh>();
	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	mesh->setAttribute(1, 2, GL_FLOAT, GL_FALSE, 0, 0, buf1);
	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount(vertices.size());

	return mesh;
}

void MoebiusStrip::makeScene()
{
	Application::makeScene();

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texCoords;
    
	double u_range_from = -0.4;
	double u_range_to = 0.4;
	double v_range_from = 0.0;
	double v_range_to = 2.0;

    for( double u = u_range_from; u < u_range_to; u += delta_u) {
        for( double v = v_range_from; v < v_range_to * glm::pi<double>(); v += delta_v ) {
            double next_v_triangle = v + delta_v > 2.0 * glm::pi<double>() ? 2.0 * glm::pi<double>() : v + delta_v;
            double next_u_triangle = u + delta_u > 0.4 ? 0.4 : u + delta_u;
            
			//Первый треугольник, образующий квад
            vertices.push_back(countVertex(u, v));
            vertices.push_back(countVertex(u, next_v_triangle));
            vertices.push_back(countVertex(next_u_triangle, v));

            normals.push_back(countNormal(u, v));
            normals.push_back(countNormal(u, next_v_triangle));
            normals.push_back(countNormal(next_u_triangle, v));
			
			//Второй треугольник, образующий квад
			vertices.push_back(countVertex(u, next_v_triangle));
            vertices.push_back(countVertex(next_u_triangle, v));
            vertices.push_back(countVertex(next_u_triangle, next_v_triangle));
			
            normals.push_back(countNormal(u, next_v_triangle));
            normals.push_back(countNormal(next_u_triangle, v));
            normals.push_back(countNormal(next_u_triangle, next_v_triangle));

			glm::vec2 texCoord1 = { (u - u_range_from) / (u_range_to - u_range_from),
				(v - v_range_from) / (v_range_to - v_range_from) };
			glm::vec2 texCoord2 = { (u - u_range_from) / (u_range_to - u_range_from),
				(next_v_triangle - v_range_from) / (v_range_to - v_range_from) };
			glm::vec2 texCoord3 = { (next_u_triangle - u_range_from) / (u_range_to - u_range_from),
				(v - v_range_from) / (v_range_to - v_range_from) };
			glm::vec2 texCoord4 = { (next_u_triangle - u_range_from) / (u_range_to - u_range_from),
				(next_v_triangle - v_range_from) / (v_range_to - v_range_from) };

            
            texCoords.push_back(texCoord1);
            texCoords.push_back(texCoord2);
            texCoords.push_back(texCoord3);
            texCoords.push_back(texCoord2);
            texCoords.push_back(texCoord3);
            texCoords.push_back(texCoord4);
        }
    }

    DataBufferPtr bufVertices = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    bufVertices->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr bufNormals = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    bufNormals->setData(normals.size() * sizeof(float) * 3, normals.data());
    
    DataBufferPtr bufTexCoords = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    bufTexCoords->setData(texCoords.size() * sizeof(float) * 2, texCoords.data());

    surface = std::make_shared<Mesh>();
    surface->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, bufVertices);
    surface->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, bufNormals);
    surface->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, bufTexCoords);

    surface->setPrimitiveType(GL_TRIANGLES);
    surface->setVertexCount(vertices.size());

    std::cout << "Surface is created with " << vertices.size() << " vertices\n";

    shader = std::make_shared<ShaderProgram>();
    shader->createProgram("./497BulgakovaData/texture.vert", "./497BulgakovaData/texture.frag");
    
    image = loadImage( "./497BulgakovaData/leo.jpg" );
    texture = loadTexture( image );

    glGenSamplers( 1, &sampler );
    glSamplerParameteri( sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
    glSamplerParameteri( sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
    glSamplerParameteri( sampler, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glSamplerParameteri( sampler, GL_TEXTURE_WRAP_T, GL_REPEAT );
}

void MoebiusStrip::updateTexture()
{
    texture = loadTexture( image );
}

void MoebiusStrip::draw()
{
    Application::draw();

    if( drawing ) {
        updateTexture();
    }
    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);

    glViewport(0, 0, width, height);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    shader->use();

    shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

    glActiveTexture( GL_TEXTURE0 );
    glBindSampler( 0, sampler );
    texture->bind();
    shader->setIntUniform("textureSampler", 0);

    shader->setMat3Uniform("normalToCameraMatrix",
                           glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * surface->modelMatrix()))));

    shader->setMat4Uniform("modelMatrix", surface->modelMatrix());

    surface->draw();

    glBindSampler(0, 0);
    glUseProgram(0);

}

void MoebiusStrip::handleKey(int key, int scancode, int action, int mods)
{
	Application::handleKey(key, scancode, action, mods);

	const double multiplier = 0.5;

	if (action == GLFW_PRESS) {
		if (key == GLFW_KEY_MINUS) {
			delta_u *= (1.0 / multiplier);
			delta_v *= (1.0 / multiplier);
			makeScene();
		}
		if (key == GLFW_KEY_EQUAL) {
			delta_u *= multiplier;
			delta_v *= multiplier;
			makeScene();
		}
	}
}

glm::vec3 MoebiusStrip::countVertex( double u, double v ) {
	double x = aa *  (cos(v) + u * cos(v / 2) * cos(v));
	double y = aa *  (sin(v) + u * cos(v / 2) * sin(v));
	double z = aa * u * sin(v / 2);
	
    return glm::vec3({x, y, z});
}

glm::vec3 MoebiusStrip::countNormal( double u, double v ) {
    double x_dev_u = aa * ( cos( v / 2 ) * cos(v) );
    double x_dev_v = aa * ( -0.5 * u * sin( v / 2 ) * cos(v) - u * sin(v) * cos( v / 2 ) - sin(v) );
    double y_dev_u = aa * ( sin(v) * cos( v / 2 ) );
    double y_dev_v = aa * ( 0.25 * u * cos( v / 2 ) + 0.75 * u * cos( 3 * v / 2 ) + cos(v) );
    double z_dev_u = aa * sin( v / 2 );
    double z_dev_v = aa * ( 0.5 * u * cos( v / 2 ) );

    glm::vec3 dev_u = glm::vec3( x_dev_u, y_dev_u, z_dev_u );
    glm::vec3 dev_v = glm::vec3( x_dev_v, y_dev_v, z_dev_v );
	
    return glm::normalize( glm::cross( dev_u, dev_v ) );
}


void MoebiusStrip::handleRightMouseButton(int action)
{
	if (action == GLFW_PRESS) {
		StartDrawing();
	}
	else if (action == GLFW_RELEASE) {
		StopDrawing();
	}
}

void MoebiusStrip::handleMouseMove(double xpos, double ypos)
{
	if (ImGui::IsMouseHoveringAnyWindow())
	{
		return;
	}

	if (drawing) {
		DrawOnSurface();
	}
	else {
		_cameraMover->handleMouseMove(_window, xpos, ypos);
	}
}

bool MoebiusStrip::findTriangle(glm::vec3 startRay, glm::vec3 endRay)
{
	if (checkCachedTriangle(startRay, endRay)) {
		return true;
	}

	bool isAnyCandidateFound = false;
	double bestCoeff = std::numeric_limits<double>::max();
	glm::vec2 bestCandidate;

	double u_range_from = -0.4;
	double u_range_to = 0.4;
	double v_range_from = 0.0;
	double v_range_to = 2.0;

	for (double u = u_range_from; u < u_range_to; u += delta_u) {
		for (double v = v_range_from; v < v_range_to; v += delta_v) {
			double next_v = v + delta_v > v_range_to ? v_range_to : v + delta_v;
			double next_u = u + delta_u > u_range_to ? u_range_to : u + delta_u;
			glm::vec3 vertex1 = countVertex(u, v);
			glm::vec3 vertex2 = countVertex(u, next_v);
			glm::vec3 vertex3 = countVertex(next_u, v);
			glm::vec3 vertex4 = countVertex(next_u, next_v);

			// Нормаль к плоскости первого треугольника
			glm::vec3 normal1 = glm::cross(vertex2 - vertex1, vertex3 - vertex1);
			glm::vec3 normal2 = glm::cross(vertex2 - vertex4, vertex3 - vertex4);

			double denominator1 = glm::dot(endRay - startRay, normal1);
			double denominator2 = glm::dot(endRay - startRay, normal2);

			// Проверяем, параллелен ли луч 1-ому треугольнику
			if (glm::abs(denominator1) >= EPSILON)
			{
				double scaleCoeff = glm::dot(normal1, vertex1 - startRay) / denominator1;
				glm::vec3 intersectionPoint = startRay + scaleVector(scaleCoeff, endRay - startRay);

				double s, t;
				if (checkIfPointInTriangle(intersectionPoint, vertex1, vertex3, vertex2, s, t)) {

					if (!isAnyCandidateFound || bestCoeff > scaleCoeff) {
						isAnyCandidateFound = true;
						bestCoeff = scaleCoeff;

						glm::vec2 texCoord = { (u - u_range_from) / (u_range_to - u_range_from),
							(v - v_range_from) / (v_range_to - v_range_from) };
						bestCandidate = addShift(texCoord, s, t);

						cachedTriangle.needReversion = false;
						cachedTriangle.vertex1 = vertex1;
						cachedTriangle.vertex2 = vertex3;
						cachedTriangle.vertex3 = vertex2;
						cachedTriangle.texCoord = texCoord;
					}
				}
			}

			// Проверяем, параллелен ли луч 2-ому треугольнику
			if (glm::abs(denominator2) >= EPSILON)
			{
				double scaleCoeff = glm::dot(normal2, vertex4 - startRay) / denominator2;
				glm::vec3 intersectionPoint = startRay + scaleVector(scaleCoeff, endRay - startRay);

				double s, t;
				if (checkIfPointInTriangle(intersectionPoint, vertex4, vertex2, vertex3, s, t)) {

					if (!isAnyCandidateFound || bestCoeff > scaleCoeff) {
						isAnyCandidateFound = true;
						bestCoeff = scaleCoeff;

						glm::vec2 texCoord = { (next_u - u_range_from) / (u_range_to - u_range_from),
							(next_v - v_range_from) / (v_range_to - v_range_from) };
						bestCandidate = addShift(texCoord, -s, -t);

						cachedTriangle.needReversion = true;
						cachedTriangle.vertex1 = vertex4;
						cachedTriangle.vertex2 = vertex2;
						cachedTriangle.vertex3 = vertex3;
						cachedTriangle.texCoord = texCoord;
					}
				}
			}
		}
	}
	if (isAnyCandidateFound) {
		hasCachedTriangle = true;
		modifyImage(bestCandidate[0], bestCandidate[1]);
	}
	return isAnyCandidateFound;
}

bool MoebiusStrip::checkCachedTriangle(glm::vec3 startRay, glm::vec3 endRay)
{
	if (!hasCachedTriangle) {
		return false;
	}
	glm::vec3 normal = glm::cross(cachedTriangle.vertex2 - cachedTriangle.vertex1,
		cachedTriangle.vertex3 - cachedTriangle.vertex1);
	double denominator = glm::dot(endRay - startRay, normal);
	if (glm::abs(denominator) >= EPSILON)
	{
		double scaleCoeff = glm::dot(normal, cachedTriangle.vertex1 - startRay) / denominator;
		glm::vec3 intersectionPoint = startRay + scaleVector(scaleCoeff, endRay - startRay);

		double s, t;
		if (checkIfPointInTriangle(intersectionPoint, cachedTriangle.vertex1, cachedTriangle.vertex2, cachedTriangle.vertex3, s, t)) {

			glm::vec2 texCoord = cachedTriangle.texCoord;
			if (cachedTriangle.needReversion) {
				texCoord = addShift(texCoord, -s, -t);
			}
			else {
				texCoord = addShift(texCoord, s, t);
			}
			modifyImage(texCoord[0], texCoord[1]);
			return true;
		}
	}
	return false;
}

glm::vec3 MoebiusStrip::scaleVector(double coeff, glm::vec3 vec)
{
	glm::vec3 result = vec;
	result.x *= coeff;
	result.y *= coeff;
	result.z *= coeff;
	return result;
}

bool MoebiusStrip::checkIfPointInTriangle(glm::vec3 intersectionPoint,
	glm::vec3 v0, glm::vec3 v1, glm::vec3 v2,
	double& s, double& t)
{
	glm::vec3 u = v1 - v0;
	glm::vec3 v = v2 - v0;
	glm::vec3 w = intersectionPoint - v0;

	double uu = glm::dot(u, u);
	double vv = glm::dot(v, v);
	double uv = glm::dot(u, v);
	double wv = glm::dot(w, v);
	double wu = glm::dot(w, u);

	/*
	* Плоскость: w = su + tv
	* Если точка в треугольнике: s,t >= 0, s + t <= 1
	*/
	s = (uv * wv - vv * wu) / (uv * uv - uu * vv);
	t = (uv * wu - uu * wv) / (uv * uv - uu * vv);

	return s >= 0.0 && t >= 0.0 && s + t <= 1.0;
}

void MoebiusStrip::modifyImage(double u, double v, glm::vec3 color)
{
	int x = image.width * u;
	int y = image.height * v;
	int startIndex = (y * image.width + x) * image.channels;
	int endIndex = startIndex + image.channels;
	for (int index = startIndex; index < endIndex; index++) {
		image.data[index] = color[index % image.channels];
	}
}

void MoebiusStrip::DrawOnSurface()
{
	double x, y, z;
	glfwGetCursorPos(_window, &x, &y);

	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);

	glm::mat4 matrix = glm::inverse(_camera.projMatrix * _camera.viewMatrix * surface->modelMatrix());

	glm::vec4 vector1;
	glm::vec4 vector2;

	vector1[0] = 2.0 * (x - viewport[0]) / viewport[2] - 1.0;
	vector1[1] = 1.0 - 2.0 * (y - viewport[1]) / viewport[3];
	vector1[2] = 0.0;
	vector1[3] = 1.0;

	vector2 = vector1;
	vector2[2] = 1.0;

	glm::vec4 position1 = matrix * vector1;
	glm::vec4 position2 = matrix * vector2;

	if (findTriangle(dividePerspective(position1), dividePerspective(position2))) {
		draw();
	}
}

glm::vec3 MoebiusStrip::dividePerspective(glm::vec4 vec)
{
	glm::vec3 result;
	result.x = vec.x / vec.w;
	result.y = vec.y / vec.w;
	result.z = vec.z / vec.w;
	return result;
}

glm::vec2 MoebiusStrip::addShift(glm::vec2 texCoord, double s, double t)
{
	double u_range_from = -0.4;
	double u_range_to = 0.4;
	double v_range_from = 0.0;
	double v_range_to = 2.0;


	texCoord[0] += s * delta_u / (u_range_to - u_range_from);
	texCoord[1] += t * delta_v / (v_range_to - v_range_from);

	texCoord[0] = std::min<float>(texCoord[0], 1.0);
	texCoord[1] = std::min<float>(texCoord[1], 1.0);
	texCoord[0] = std::max<float>(texCoord[0], 0.0);
	texCoord[1] = std::max<float>(texCoord[1], 0.0);
	return texCoord;
}