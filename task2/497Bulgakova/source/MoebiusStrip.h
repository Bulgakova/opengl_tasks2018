#include <Application.hpp>
#include <cmath>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <iostream>
#include <algorithm>
#include <chrono>


class MoebiusStrip : public Application
{
public:
    MoebiusStrip() : delta_u(0.1), delta_v(0.1), aa(1.0), drawing(false), hasCachedTriangle(false) {}

	MeshPtr makeMirror() override;
    void makeScene() override;
    void update() override { Application::update(); }
    void draw() override;
    void handleKey(int key, int scancode, int action, int mods) override;

	void handleRightMouseButton(int action) override;

	void handleMouseMove(double xpos, double ypos) override;

	void StartDrawing() { drawing = true; }
	void StopDrawing() { drawing = false; hasCachedTriangle = false; }
	bool isDrawing() { return drawing; }

	void DrawOnSurface();

private:
	bool hasCachedTriangle;
	struct {
		glm::vec3 vertex1;
		glm::vec3 vertex2;
		glm::vec3 vertex3;
		glm::vec2 texCoord;
		bool needReversion;
	} cachedTriangle;

	static const double EPSILON;

	glm::vec3 countVertex(double u, double v);
	glm::vec3 countNormal(double u, double v);

	bool findTriangle(glm::vec3 startRay, glm::vec3 endRay);
	void modifyImage(double u, double v, glm::vec3 color = glm::vec3({ 0, 0, 0 }));
	void updateTexture();

	glm::vec3 scaleVector(double coeff, glm::vec3 vec);
	bool checkIfPointInTriangle(glm::vec3 intersectionPoint,
		glm::vec3 v0, glm::vec3 v1, glm::vec3 v2,
		double& s, double& t);
	glm::vec3 dividePerspective(glm::vec4 vec);

	inline double getNextU(double u) const;
	inline double getNextV(double v) const;

	glm::vec2 addShift(glm::vec2 texCoord, double s, double t);
	bool checkCachedTriangle(glm::vec3 startRay, glm::vec3 endRay);

	MeshPtr surface;
	ShaderProgramPtr shader;

	TextureImage image;
	TexturePtr texture;
	GLuint sampler;

	bool drawing;

	float delta_u;
	float delta_v;
	float aa;
};
