/*
Получает на вход интеполированный цвет фрагмента и копирует его на выход.
*/

#version 330

uniform sampler2D textureSampler;

in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

void main()
{
	fragColor = vec4( texture(textureSampler, texCoord).rgb, 1.0);
}